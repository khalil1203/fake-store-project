document.querySelector(".loader").style.display = 'block';

fetch('https://fakestoreapi.com/products')
    .then(res => res.json())
    .then((data) => {
        document.querySelector(".loader").style.display = 'none';
        if (data.length == 0) {
            document.querySelector('.data').innerHTML = `<h1 class="error">No data available!!!!</h1>`;
        }
        else {
            data.map((entries) => {
                let datamap =
                    `<div class="card">
                <img src="${entries.image}"></img>
                
                <h2>${entries.title}</h2>
                <h2><b>Price : $</b>${entries.price}</h2>
                <div class="card-data">
                <div><b>Category :</b> ${entries.category}</div>
                <div><b>Description :</b>${entries.description.length < 100 ? entries.description : entries.description.substr(0, 97) + "..."}</div>
        
                <div class="rating">
                <div><b><i class="fa-sharp fa-solid fa-face-smile"></i> : </b>${getStars(entries.rating.rate)}</div>
                <div><b><i class="fa-solid fa-user"></i> : </b>${entries.rating.count}</div>
                </div>
                </div>
                
                </div>`
                document.querySelector('.data').innerHTML += (datamap);
            });
        }


    })
    .catch((err) => {
        if (err) {
            document.querySelector('.data').innerHTML = `<h1 class="error">Error Cannnot connect to server!!!!</h1>`;
        }
    });



function getall() {
    console.log("getting all data");
    document.querySelector('.data').innerHTML = "";

    fetch('https://fakestoreapi.com/products')
        .then(res => res.json())
        .then((data) => {
            if (data.length == 0) {
                document.querySelector('.data').innerHTML = `<h1 class="error">No data available!!!!</h1>`;
            }
            else {
                data.map((entries) => {

                    let datamap =
                        `<div class="card">
                    <img src="${entries.image}"></img>
                    
                    <h2>${entries.title}</h2>
                    <h2><b>Price : $</b>${entries.price}</h2>
                    <div class="card-data">
                    <div><b>Category :</b> ${entries.category}</div>
                    <div><b>Description :</b>${entries.description.length < 100 ? entries.description : entries.description.substr(0, 97) + "..."}</div>
            
                    <div class="rating">
                    <div><b><i class="fa-sharp fa-solid fa-face-smile"></i> : </b>${getStars(entries.rating.rate)}</div>
                    <div><b><i class="fa-solid fa-user"></i> : </b>${entries.rating.count}</div>
                    </div>
                    </div>
                    
                    </div>`
                    document.querySelector('.data').innerHTML += (datamap);
                });
            }
        })
        .catch((err) => {
            if (err) {
                document.querySelector('.data').innerHTML = `<h1 class="error">Error Cannnot connect to server!!!!</h1>`;
            }
        });

}

function getparticular() {
    document.querySelector('.data').innerHTML = "";
    let link = document.querySelector('input').value;
    link = link.toLowerCase();
    fetch(`https://fakestoreapi.com/products`)
        .then(res => res.json())
        .then((data) => {
            let arr = data.filter((entries) => {
                let lower = entries.title.toLowerCase();
                return lower.split(" ").includes(link);
            });
            if (arr.length) {

                arr.map((entries) => {

                    console.log(entries);



                    let datamap =
                        `<div class="card">
                        <img src="${entries.image}"></img>
                        
                        <h2>${entries.title}</h2>
                        <div class="grey">
                        <h2><b>Price : $</b>${entries.price}</h2>
                        <div><b>Category :</b> ${entries.category}</div>
                        <div class="rating">
                        <div><b><i class="fa-sharp fa-solid fa-face-smile"></i> : </b>${getStars(entries.rating.rate)}</div>
                        <div><b><i class="fa-solid fa-user"></i> : </b>${entries.rating.count}</div>
                        </div>
                        </div>
                        </div>`


                    document.querySelector('.data').innerHTML += (datamap);
                });
            }
            else {
                document.querySelector('.data').innerHTML = `<h1 class="error">No data available!!!!</h1>`;
            }

        })

        .catch((err) => {
            if (err) {
                return "Sorry you cannot proceed from here!!!!";
            }
        });


}


function getcat(str) {
    console.log("getting all data");
    document.querySelector('.data').innerHTML = "";
    fetch('https://fakestoreapi.com/products')
        .then(res => res.json())
        .then((data) => {
            if (data.length == 0) {
                document.querySelector('.data').innerHTML = `<h1 class="error">No data available!!!!</h1>`;
            }
            else {
                data.filter((entries) => {
                    return entries.category == str;
                }).map((entries) => {
                    let datamap =
                        `<div class="card">
                    <img src="${entries.image}"></img>
                    
                    <h2>${entries.title}</h2>
                    <h2><b>Price : $</b>${entries.price}</h2>
                    <div class="card-data">
                    <div><b>Category :</b> ${entries.category}</div>
                    <div><b>Description :</b>${entries.description.length < 100 ? entries.description : entries.description.substr(0, 97) + "..."}</div>
            
                    <div class="rating">
                    <div><i class="fa-sharp fa-solid fa-face-smile"></i> : ${getStars(entries.rating.rate)}</div>
                    <div><b><i class="fa-solid fa-user"></i> : </b>${entries.rating.count}</div>
                    </div>
                    </div>
                    
                    </div>`


                    document.querySelector('.data').innerHTML += (datamap);
                });
            }
        })
        .catch((err) => {
            if (err) {
                return "Sorry you cannot proceed from here!!!!";
            }
        });

}

function getStars(rating) {

    // Round to nearest half
    rating = Math.round(rating * 2) / 2;
    let output = [];
    let index2 = 0;
    // Append all the filled whole stars
    for (let index1 = rating; index1 >= 1; index1--) {
        index2 = index1;
        output.push('<i class="fa fa-star" aria-hidden="true" style="color: gold;"></i>&nbsp;');
    }
    // If there is a half a star, append it
    if (index2 > 1) output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

    // Fill the empty stars
    for (let index3 = (5 - rating); index3 >= 1; index3--)
        output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

    return output.join('');

}
