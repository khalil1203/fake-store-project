function containsNumbers(str) {
  return /[^a-zA-Z]/.test(str);
}

function containemail(str){
  return /[@]/.test(str);
}


document.getElementById("signupForm").addEventListener("submit", function (event) {
  event.preventDefault(); 

  let firstName = document.getElementById("firstName").value;
  let lastName = document.getElementById("lastName").value;
  let email = document.getElementById("email").value;
  let password = document.getElementById("password").value;
  let confirmPassword = document.getElementById("confirmPassword").value;
  let tosChecked = document.getElementById("tos").checked;

 let valid1 = true;
 let valid2 = true;
 let valid3 = true;
 let valid4 = true;
 let valid5 = true;
 let valid6 = true;
 let valid7 = true;

  let firstnamChecked = firstName.split(" ").join("");
  let lastnamChecked = lastName.split(" ").join("");
  let errorMessages = [];
  


  if (firstnamChecked.length == 0 || containsNumbers(firstnamChecked)) {
    valid1 = false;
    document.querySelector(".red0").style.display = 'block'
    document.querySelector(".red0").textContent = 'Provide correct Name!';
  }
  if(firstnamChecked && !containsNumbers(firstnamChecked)){
    valid1 = true;
    document.querySelector(".red0").style.display = 'none';
  }
  



  if (!lastName || lastnamChecked.length == 0 || containsNumbers(lastName)) {
    valid2 = false;
    document.querySelector(".red1").style.display = 'block';
    document.querySelector(".red1").textContent = 'Provide correct Name!'; 
  }
  if(lastnamChecked && !containsNumbers(lastnamChecked)){
    valid2 = true;
    document.querySelector(".red1").style.display = 'none';
  }




  if (!email || !containemail(email)) {
    valid3 = false;
    document.querySelector(".red2").style.display = 'block';
    document.querySelector(".red2").textContent = 'Provide correct Email!';
  }
  if(email && containemail(email)){
    valid3 = true;
    document.querySelector(".red2").style.display = 'none';
  }




  
  if (!password) {
    valid4 = false;
    document.querySelector(".red3").style.display = 'block'
    
  }
  if (!confirmPassword) {
    valid5 = false;
    document.querySelector(".red4").style.display = 'block'
  }


  if (password !== confirmPassword) {
    valid6 = false;
    document.querySelector(".red4").style.display = 'block'
    document.querySelector(".red4").textContent = 'Password not Matching!';
  }
  else{
    valid6 = true;
    document.querySelector(".red4").style.display = 'none';
  }
  


  if (!tosChecked) {
    valid7 = false;
    document.querySelector(".red5").style.display = 'block'
    
  }
  if (!valid1 || !valid2 || !valid3 || !valid4 || !valid5 || !valid6 || !valid7) {
    console.log("error");
  } else {
    errorContainer.innerHTML = "";
    window.location.href = "index.html";
  }
});
